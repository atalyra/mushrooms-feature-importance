#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  3 23:01:25 2023

@author: ata
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from math import log2
column_names = ['class',
                'cap-shape',
                'cap-surface',
                'cap-color',
                'bruises?',
                'odor',
                'gill-attachment',
                'gill-spacing',
                'gill-size',
                'gill-color',
                'stalk-shape',
                'stalk-root',
                'stalk-surface-above-ring',
                'stalk-surface-below-ring',
                'stalk-color-above-ring',
                'stalk-color-below-ring',
                'veil-type',
                'veil-color',
                'ring-number',
                'ring-type',
                'spore-print-color',
                'population',
                'habitat']

url = 'https://archive.ics.uci.edu/ml/machine-learning-databases/mushroom/agaricus-lepiota.data'
df= pd.read_csv(url, header=None, names=column_names)
df.head()
df["class"].unique()
df.columns


from scipy.stats import entropy
labels=df["class"]
def entropy1(labels, base=2):
  value,counts = np.unique(labels, return_counts=True)
  return entropy(counts, base=base)

print(np.unique(df["class"],return_counts=True))

print(entropy1(labels))
target_entropy=entropy1(labels)
#entropy(np.array(df["class"]))
df['gill-color'].unique()
# calculate the information gain for each column
information_gain = {}
for column in df.columns[:-1]:
    column_entropy = 0
    levels = df[column].drop_duplicates().tolist()

    for level in levels:
        level_prob = df[df[column] == level].shape[0] / df.shape[0]
        target_probs = df[df[column] == level]['class'].value_counts() / df[df[column] == level].shape[0]
        level_entropy =  0
        for target_level, target_prob in target_probs.items():
            level_entropy  -= target_prob * log2(target_prob)
        column_entropy += level_prob * level_entropy
    information_gain[column] = target_entropy - column_entropy

print(information_gain)



#{'class': 0.9990678968724604, 'cap-shape': 0.04879670193537311, 'cap-surface': 0.028590232773772817, 'cap-color': 0.03604928297620391, 'bruises?': 0.19237948576121966, 'odor': 0.9060749773839999, 'gill-attachment': 0.014165027250616302, 'gill-spacing': 0.10088318399657037, 'gill-size': 0.23015437514804615, 'gill-color': 0.41697752341613137, 'stalk-shape': 0.007516772569664321, 'stalk-root': 0.13481763762727572, 'stalk-surface-above-ring': 0.2847255992184845, 'stalk-surface-below-ring': 0.2718944733927464, 'stalk-color-above-ring': 0.2538451734622399, 'stalk-color-below-ring': 0.24141556652756657, 'veil-type': 1.1102230246251565e-16, 'veil-color': 0.0238170161209168, 'ring-number': 0.03845266924309054, 'ring-type': 0.3180215107935377, 'spore-print-color': 0.4807049176849154, 'population': 0.2019580190668524}

